// by 癫癫博士
// 实现自定义戳一戳交互功能，支持返回文字图片语音禁言，其中语音需配置ffmpeg
// 希望大家喜欢^^
//本人没有测试群，不过其他大佬有（比如渔火佬仓库里的其他作者），可以去看看，我也在里面快乐水群^^
//有idea/有提议/发现bug的话可以通过gitee评论或者私聊联系我~
//项目gitee地址：https://gitee.com/huangshx2001/yunzai-js-plug-in
//欢迎来找找其他有趣的项目或者来点个star~


import plugin from'../../lib/plugins/plugin.js'
import{segment}from'oicq'
import cfg from'../../lib/config/config.js'
import common from'../../lib/common/common.js'
const path=process.cwd()


//在这里设置事件概率,请保证概率加起来小于1，少于1的部分会触发反击
let reply_text = 0.35 //文字回复概率
let reply_img = 0.25 //图片回复概率
let reply_voice = 0.2 //语音回复概率
let mutepick = 0.05 //禁言概率
let example = 0.07 //拍一拍表情概率
//剩下的0.08概率就是反击


//定义图片存放路径 默认是Yunzai-Bot/resources/chuochuo
const chuo_path=path+'/resources/chuochuo/';


//图片需要从1开始用数字命名并且保存为jpg或者gif格式，存在Yunzai-Bot/resources/chuochuo目录下
let jpg_number = 34 //输入jpg图片数量
let gif_number = 2 //输入gif图片数量



//回复文字列表
let word_list=['回复1',
'回复2',
'回复3',
'回复4'];


//回复语音列表 默认是芭芭拉的语音 可以复制到网址里面去改，然后再复制回来 
//语音合成来源：https://github.com/w4123/vits
//接口格式参考：http://233366.proxy.nscc-gz.cn:8888/?text=你好&speaker=派蒙
//原列表语音：
//你戳谁呢！你戳谁呢！！！
//不要再戳了！我真的要被你气死了！！！
//怎么会有你这么无聊的人啊！！！
//是不是要柚恩揍你一顿才开心啊！！！
//不要再戳了！！！
//讨厌死了！
//小朋友别戳了
//旅行者副本零掉落，旅行者深渊打不过，旅行者抽卡全保底，旅行者小保底必歪
let voice_list = [`http://233366.proxy.nscc-gz.cn:8888/?text=%E4%BD%A0%E6%88%B3%E8%B0%81%E5%91%A2%EF%BC%81%E4%BD%A0%E6%88%B3%E8%B0%81%E5%91%A2%EF%BC%81%EF%BC%81%EF%BC%81&speaker=%E8%8A%AD%E8%8A%AD%E6%8B%89`,
`http://233366.proxy.nscc-gz.cn:8888/?text=%E4%B8%8D%E8%A6%81%E5%86%8D%E6%88%B3%E4%BA%86%EF%BC%81%E6%88%91%E7%9C%9F%E7%9A%84%E8%A6%81%E8%A2%AB%E4%BD%A0%E6%B0%94%E6%AD%BB%E4%BA%86%EF%BC%81%EF%BC%81%EF%BC%81&speaker=%E8%8A%AD%E8%8A%AD%E6%8B%89`,
`http://233366.proxy.nscc-gz.cn:8888/?text=%E6%80%8E%E4%B9%88%E4%BC%9A%E6%9C%89%E4%BD%A0%E8%BF%99%E4%B9%88%E6%97%A0%E8%81%8A%E7%9A%84%E4%BA%BA%E5%95%8A%EF%BC%81%EF%BC%81%EF%BC%81&speaker=%E8%8A%AD%E8%8A%AD%E6%8B%89`,
`http://233366.proxy.nscc-gz.cn:8888/?text=%E6%98%AF%E4%B8%8D%E6%98%AF%E8%A6%81%E6%9F%9A%E6%81%A9%E6%8F%8D%E4%BD%A0%E4%B8%80%E9%A1%BF%E6%89%8D%E5%BC%80%E5%BF%83%E5%95%8A%EF%BC%81%EF%BC%81%EF%BC%81&speaker=%E8%8A%AD%E8%8A%AD%E6%8B%89&length=1`,
`http://233366.proxy.nscc-gz.cn:8888/?text=%E4%B8%8D%E8%A6%81%E5%86%8D%E6%88%B3%E4%BA%86%EF%BC%81%EF%BC%81%EF%BC%81&speaker=%E8%8A%AD%E8%8A%AD%E6%8B%89`,
`http://233366.proxy.nscc-gz.cn:8888/?text=%E8%AE%A8%E5%8E%8C%E6%AD%BB%E4%BA%86%EF%BC%81&speaker=%E8%8A%AD%E8%8A%AD%E6%8B%89`,
`http://233366.proxy.nscc-gz.cn:8888/?text=%E5%B0%8F%E6%9C%8B%E5%8F%8B%E5%88%AB%E6%88%B3%E4%BA%86&speaker=%E8%8A%AD%E8%8A%AD%E6%8B%89`,
`http://233366.proxy.nscc-gz.cn:8888/?text=%E6%97%85%E8%A1%8C%E8%80%85%E5%89%AF%E6%9C%AC%E9%9B%B6%E6%8E%89%E8%90%BD%EF%BC%8C%E6%97%85%E8%A1%8C%E8%80%85%E6%B7%B1%E6%B8%8A%E6%89%93%E4%B8%8D%E8%BF%87%EF%BC%8C%E6%97%85%E8%A1%8C%E8%80%85%E6%8A%BD%E5%8D%A1%E5%85%A8%E4%BF%9D%E5%BA%95%EF%BC%8C%E6%97%85%E8%A1%8C%E8%80%85%E5%B0%8F%E4%BF%9D%E5%BA%95%E5%BF%85%E6%AD%AA&speaker=%E8%8A%AD%E8%8A%AD%E6%8B%89`]

export class chuo extends plugin{
    constructor(){
    super({
        name: '戳一戳',
        dsc: '戳一戳机器人触发效果',
        event: 'notice.group.poke',
        priority: 5000,
        rule: [
            {
                /** 命令正则匹配 */
                fnc: 'chuoyichuo'
                }
            ]
        }
    )
}


async chuoyichuo (e){
    logger.info('[戳一戳生效]')
    if(e.target_id == cfg.qq){
        //生成0-100的随机数
        let random_type = Math.random()
        
        //回复随机文字
        if(random_type < reply_text){
            let text_number = Math.ceil(Math.random() * word_list['length'])
            await e.reply(word_list[text_number-1])
        }
        
        
        //回复随机图片
        else if(random_type < (reply_text + reply_img)){

            let photo_number = Math.ceil(Math.random() * (jpg_number + gif_number))
            
            if(photo_number<=jpg_number){
                e.reply(segment.image('file:///' + path + '/resources/chuochuo/'+ photo_number + '.jpg'))
            }
            else{
                photo_number = photo_number - jpg_number
                e.reply(segment.image('file:///' + path + '/resources/chuochuo/'+ photo_number + '.gif'))
            }

        }
        
        //回复随机语音
        else if(random_type < (reply_text + reply_img + reply_voice)){
            let voice_number = Math.ceil(Math.random() * word_list['length'])
            let url = voice_list[voice_number-1]
            await e.reply(segment.record(url))
        }
        
        //禁言
        else if(random_type < (reply_text + reply_img + reply_voice + mutepick)){
            //两种禁言方式，随机选一种
            let mutetype = Math.ceil(Math.random() * 2)
            if(mutetype == 1){
                e.reply('说了不要戳了！')
                await common.sleep(1000)
                await e.group.muteMember(e.operator_id,60);
                await common.sleep(3000)
                e.reply('啧')
                //有这个路径的图话可以加上
                //await e.reply(segment.image('file:///' + path + '/resources/chuochuo/'+'laugh.jpg'))
            }
            else if (mutetype == 2){
                e.reply('不！！')
                await common.sleep(500);
                e.reply('准！！')
                await common.sleep(500);
                e.reply('戳！！')
                await common.sleep(1000);
                await e.group.muteMember(e.operator_id,60)
            }
        }
        
        //拍一拍表情包
        else if(random_type < (reply_text + reply_img + reply_voice + mutepick + example)){
            await e.reply(await segment.image(`http://ovooa.com/API/face_pat/?QQ=${e.operator_id}`))
        }
        
        //反击
        else {
            e.reply('反击！')
            await common.sleep(1000)
            await e.group.pokeMember(e.operator_id)
        }
        
    }
    
}
    
}